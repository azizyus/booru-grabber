import aiohttp
from aiohttp_socks import SocksConnector, SocksVer

def makeForProxy(address=None,port=9999,redirectDns=False):
    connector  = SocksConnector(
        socks_ver=SocksVer.SOCKS5,
        host=address,
        port=port,
        username=None,
        password=None,
        rdns=redirectDns
    )
    return connector


def make():
    connector = aiohttp.TCPConnector()
    return connector


def map(value,address=None,port=None,redirectDns=False):
    if value == True:
        return makeForProxy(address,port,redirectDns)
    else:
        return make()


def detect(value,address=None,port=None,redirectDns=False):
        return map(value,address,port,redirectDns)
import os


class ApiImageModel():

    def __init__(self, image,savePath):
        self.file_url = image["file_url"]
        self.fileName = os.path.basename(self.file_url)
        self.savePath = savePath
        self.id = image["id"]

    def getFileSavePath(self):
        return self.savePath+"/"+self.fileName

    def isFileExist(self):
        fileSavePath = self.getFileSavePath()
        return os.path.isfile(fileSavePath)


def makeApiImageModel(image,savePath):
    return ApiImageModel(image,savePath)
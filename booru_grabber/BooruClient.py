from booru_grabber.UrlBuilder import UrlBuilder
import json
import os
from booru_grabber.HttpClient import HttpClient
import asyncio
from booru_grabber.Logger import Logger
import datetime
from booru_grabber.exceptions import UserStoppedException
from booru_grabber.ApiImageModel import makeApiImageModel

class ApiClient:


    def __init__(self):
         self.client = HttpClient()
         self.fileNameKey = "file_url"
         self.downloadDirectory = "."
         self.override = False
         self.limit = 0

    def setLogger(self,logger=Logger):
        self.logger = logger
        self.client.setLogger(self.logger)

    def setUrlBuilder(self,urlBuilder=UrlBuilder):
        self.urlBuilder = urlBuilder

    def downloadUntilEnds(self):
        while True:

            itemCount = self.listingPageRequest()
            self.urlBuilder.increasePage()
            if itemCount == 0:
                break

    def mapIntoEntity(self,decodedJsonHasMap):
        c = list()
        for x in decodedJsonHasMap:
            c.append(makeApiImageModel(x,"."))
        return  c

    def listingPageRequest(self):

        buildedUrl = self.urlBuilder.buildFromCurrentJson()


        listingPageResponse = self.client.sendGetRequestAndGetResult(buildedUrl)

        if listingPageResponse.code == 200 and listingPageResponse.isSuccess:
            resultHtml = listingPageResponse.text
            jsonDecode = json.loads(resultHtml)
            resultDecoded = self.mapIntoEntity(jsonDecode)
            itemCount = len(jsonDecode)
            self.limit = len(list(filter(self.checkIsNotDownloaded,resultDecoded)))
            self.counter = 0
            # print(listingPageRequest.text)
            if itemCount > 0:
                # pprint(resultDecoded)
                loop = asyncio.get_event_loop()
                tasks = []
                startTime = datetime.datetime.now()
                self.logger.logHeader("################ PAGE "+str(self.urlBuilder.page)+" started ["+str(datetime.datetime.now())+"] #################")
                self.initCounter()
                for image in resultDecoded:
                    # await self.downloadImageAndSave(image,self.client)
                    tasks.append(  self.downloadImageAndSave(image,self.client))
                    # newThread = threading.Thread(target=self.downloadImageAndSave,args=(image,self.client))
                    # newThread.start()

                try:
                       loop.run_until_complete(asyncio.wait(tasks))
                except KeyboardInterrupt as e:
                    UserStoppedException.throwStopByUserException()

                diff = (datetime.datetime.now() - startTime)
                now = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S")
                self.logger.logHeader("################ PAGE " + str(self.urlBuilder.page) + " finished ["+str(now)+" - "+str(diff.seconds)+" second tooks] #################")
                return itemCount
        else:
            self.logger.logMessage("No response recieved from server? are you sure you are using right url or check ethernet connection?")


        return 0

    def checkIsNotDownloaded(self,image):
        return not image.isFileExist()

    async def increaseCounter(self):
        self.counter+=1
        self.logger.logStatus(str(self.counter)+"/"+str(self.limit))

    def initCounter(self):
        if(self.limit == 0):
            self.logger.logMessage("No new image found in this page")
        else:
            self.logger.logStatus("0/"+str(self.limit))

    async def downloadImageAndSave(self, image, client):
                if image.file_url != "":
                    if not image.isFileExist() or self.override:
                        self.logger.log("Started: " + image.file_url)
                        fileData = await client.sendRequestBinary(image.file_url)
                        f = open(image.getFileSavePath(), "ab")
                        f.write(fileData)
                        f.close()
                        await self.increaseCounter()
                        #self.logger.log("Completed: " + imageUrl)

                    else:
                        self.logger.debugLog("This file is already exist, passing: " + image.file_url)
                else:
                    self.logger.debugLog("the file you requested doesnt exist, id is:  "+str(image.id)+" ==> skipped")





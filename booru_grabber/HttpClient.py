import requests
from booru_grabber.Logger import Logger
from booru_grabber.HttpResult import HttpResult
from booru_grabber.exceptions import UserStoppedException
import aiohttp
from booru_grabber import AioHttpConnectorFactory


class HttpClient():

    def __init__(self):
        self.requests = requests
        self.logger = Logger()
        self.hasProxy = False
        self.proxyHost = None
        self.proxyPort = None
        self.redirectDns = False


    def setLogger(self,logger=Logger):
        self.logger = logger


    def sendGetRequestAndGetHtml(self, url):
        return self.sendRequestSync("GET",url).text


    def sendGetRequestAndGetBytes(self,url):
        return self.sendRequestSync("GET",url).data

    def sendGetRequestAndGetResult(self,url):
        return self.sendRequestSync("GET",url)

    def sendRequestSync(self,type,url):
        httpResult = HttpResult()
        try:

            httpResult.setResult(self.requests.request(type, url))
            httpResult.setIsSuccess(True)
            self.httpLog(httpResult,url)
        except KeyboardInterrupt as e:
              UserStoppedException.throwStopByUserException()
        return httpResult


    def httpLog(self,httpResult=HttpResult,url=""):
        self.logger.debugLog("Completed [HTTP:"+str(httpResult.code)+"] "+url)

    async def sendRequestHtml(self,url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                httpResult = HttpResult()
                httpResult.text = await response.text()
                httpResult.code = response.status
                httpResult.isSuccess = True
                self.httpLog(httpResult, url)
                return httpResult.text

    async def sendRequestBinary(self, url):
          connector = AioHttpConnectorFactory.detect(self.hasProxy, self.proxyHost, self.proxyPort, self.redirectDns)
          async with aiohttp.ClientSession(connector=connector) as session:
                    async with session.get(url) as response:
                        httpResult = HttpResult()
                        httpResult.data = await response.read()
                        httpResult.code = response.status
                        httpResult.isSuccess = True
                        self.httpLog(httpResult, url)
                        return httpResult.data


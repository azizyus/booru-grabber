

class HttpResult:

        def __init__(self):
            self.result = None
            self.code = None
            self.text = None
            self.data = None
            self.isSuccess = False



        def setResult(self,result):
            self.result = result
            self.code = result.status_code
            self.text = result.text
            self.data = result.content

        def setIsSuccess(self,value):
            self.isSuccess = value

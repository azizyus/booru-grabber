import sys


class Logger:


    def __init__(self):
        self.isLogActive = True
        self.isDebugLogActive = False

    def log(self,data):
        if self.isDebugLogActive:
            print("\n"+data)

    def logHeader(self,data):
        print("\n"+data)


    def logMessage(self,data):
        print("\n"+data)

    def debugLog(self,data):
        if self.isDebugLogActive:
            print(data)

    def logStatus(self,data):
               sys.stdout.write('\r')
               sys.stdout.write(data)
               sys.stdout.flush()


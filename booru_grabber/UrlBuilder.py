from booru_grabber.ArrayHelper import Helper
from urllib.parse import urlparse,parse_qs,urlencode
from copy import deepcopy

class UrlBuilder:

    def __init__(self):
        self.tagsQueryName = "tags"
        self.pageQueryName = "page"



    def setUrl(self,startUrl):
        self.parsedUrl = urlparse(startUrl)
        self.protocol = self.parsedUrl.scheme
        self.baseUrl = self.parsedUrl.netloc
        self.path = self.parsedUrl.path
        self.query = parse_qs(self.parsedUrl.query)
        self.tags = Helper.getOrDefault(self.query, self.tagsQueryName, "")
        self.page, = Helper.getOrDefault(self.query, self.pageQueryName, [1])
        self.page = int(self.page)

    def increasePage(self):
        self.page += 1

    def buildFrom(self,UrlBuilder):
        url = UrlBuilder.protocol+"://"+UrlBuilder.baseUrl+UrlBuilder.path
        url+="?"
        url+= urlencode({UrlBuilder.tagsQueryName:",".join(UrlBuilder.tags),UrlBuilder.pageQueryName:UrlBuilder.page})
        return url

    def buildFromCurrentJson(self):
        currentInstance = deepcopy(self)
        currentInstance.path+=".json" #we should always capable of retrieving original builder
        return self.buildFrom(currentInstance)

    def buildFromCurrent(self):
        return self.buildFrom(self)

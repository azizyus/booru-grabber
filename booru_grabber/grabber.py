from booru_grabber.BooruClient import ApiClient
from booru_grabber.UrlBuilder import UrlBuilder
import sys
import os
import requests
from booru_grabber.Logger import Logger

def main():


    startUrl = ""
    if len(sys.argv) > 1:
        startUrl = sys.argv[1]

    elif len(sys.argv) <= 1:
        print("please provide link")
        exit(0)

    myParser = ApiClient()
    urlBuilder = UrlBuilder()
    urlBuilder.setUrl(startUrl)
    myParser.setUrlBuilder(urlBuilder)
    newLogger = Logger()
    newLogger.isLogActive = True
    newLogger.isDebugLogActive = False
    myParser.setLogger(newLogger)

    #OPTIONS#
    if "--version" in sys.argv:
        print("0.4.1")
        return
    if "--override" in sys.argv:
        myParser.override = True
    if "--auto-proxy" in sys.argv:
        proxyFullHost = os.environ["all_proxy"]
        (scheme,fullAddress) = proxyFullHost.split("://")
        print(scheme,fullAddress)
        (address,port) = fullAddress.split(":")
        # print(address,port)
        myParser.client.proxyHost = address
        myParser.client.proxyPort = port
        myParser.client.hasProxy = True
        if scheme == "socks5h":
            myParser.client.redirectDns = True
        elif scheme == "socks5":
            myParser.client.redirectDns = False
    #OPTIONS#

    try:
        myParser.downloadUntilEnds()
    except requests.exceptions.ConnectionError as e:
        print("Connection error, check your url or ethernet connection: ")


## DOC

#### WHATS PURPOSE?

Downloading every chinese image from specified booru, it will download all pages in specified url.

#### EXAMPLE USAGE

<pre>booru-grabber "xx.com/posts?tag=kek"</pre> <br>

#### WITH PROXY
<pre>booru-grabber "xx.com/posts?tag=kek" --auto-proxy</pre> <br>
The <pre>--auto-proxy</pre> flag reads your $all_proxy variable and 
inits connector with that proxy address, if you want to redirect dns use socks5h:// otherwise socks5:// is fine

#### PIP INSTALLATION

pip3 install . --user
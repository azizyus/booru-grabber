#!/usr/bin/python3

""" setup.py for mps-youtube.

https://np1.github.com/mps-youtube

python setup.py sdist bdist_wheel
"""

import sys
import os

if sys.version_info < (3,0):
    sys.exit("booru-grabber python 3.")

from setuptools import setup
from setuptools import find_packages
VERSION = "0.4.1"

options = dict(
    name="booru-grabber",
    version=VERSION,
    description="Terminal based booru-grabber",
    author="azizyus",
    author_email="canazizoglu97@gmail.com",
    url="https://gitlab.com/azizyus/booru-grabber-python.git",
    entry_points={'console_scripts': ['booru-grabber = booru_grabber:grabber.main']},
    packages=['booru_grabber','booru_grabber.exceptions'],
)

setup(**options)
